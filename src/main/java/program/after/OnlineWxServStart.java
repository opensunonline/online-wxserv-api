package program.after;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ApplicationContext;

import program.faceapi.base.api.SpringContextUtil;
/**
 * online-wxserv-api启动
 * @author yangyang.zhang
 * @Package program.after 
 * @Date 2018年4月5日 下午1:20:11 
 * @Description TODO(用一句话描述该文件做什么)
 * @version V1.0
 */
@SpringBootApplication  //该注释是表示可以解析Spring类的上下文配置信息
@EnableEurekaClient //使用注册中心
@EnableFeignClients(basePackages={"program.faceapi"})  //使用 Feign 客户端  basePackages 可以指定其他包下的接口类;  注意basePackages配置的包的路径下必须有java类
public class OnlineWxServStart {
    
	private static Logger logger =LoggerFactory.getLogger(OnlineWxServStart.class);

    public static void main(String[] args) throws Exception {
    	logger.info("Spring Boot start.........."); 
    	ApplicationContext ctx = SpringApplication.run(OnlineWxServStart.class, args);
		//注入applicationContext
		SpringContextUtil.setApplicationContext(ctx);
    }
    
    
}
