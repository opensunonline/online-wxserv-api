package program.after.service;

import program.after.msg.in.InMsg;
import program.after.msg.out.OutMsg;

public interface MsgSelectService {

	public OutMsg sendServiceApi(String appId, InMsg inMsg); 
	
}
