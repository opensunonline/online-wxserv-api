package program.after.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import program.after.controller.WechatMsgController;
import program.after.entity.repository.ServiceConfigDao;
import program.after.entity.wxapi.ServiceConfig;
import program.after.msg.in.InMsg;
import program.after.msg.in.InNotDefinedMsg;
import program.after.msg.in.InTextMsg;
import program.after.msg.in.event.InNotDefinedEvent;
import program.after.msg.in.event.InQrCodeEvent;
import program.after.msg.out.OutMsg;
import program.after.msg.out.OutTextMsg;
import program.after.remote.RemoteApiFactoryImpl;
import program.after.remote.RemoteApiSelectImpl;
import program.after.util.OutMsgParse;
import program.faceapi.base.ServiceResult;

@Service
public class MsgSelectServiceImpl implements MsgSelectService {

	private static Logger logger = LoggerFactory.getLogger(MsgSelectServiceImpl.class);
	
	@Autowired
	private RemoteApiSelectImpl remoteApiSelectImpl;

	@Autowired
	private RemoteApiFactoryImpl remoteApiFactoryImpl;

	@Autowired
	private ServiceConfigDao serviceConfigDao;

	private Map<String, ServiceConfig> servConfig = new HashMap<>();

	@Override
	public OutMsg sendServiceApi(String appId, InMsg inMsg) {
		// TODO Auto-generated method stub
		logger.info("sendServiceApi开始执行------->appId=[" + appId + "]");
		ServiceConfig config = servConfig.get(appId);
		if (config == null) {
			config = serviceConfigDao.findByAppId(appId);
			if (config == null) {
				return null;
			}
			servConfig.put(appId, config);
		}
		//判断该消息类型是否转发继续处理
		if (!OutMsgParse.isSend(inMsg, config))
			return null;
		String cmmd = config.getApiName() + ":" + config.getServiceName();
		logger.info("cmmd----------->" + cmmd);
		JSONObject param = new JSONObject();
		param.put("msgType", inMsg.getMsgType());
		param.put("appId", appId);
		param.put("data", JSON.toJSONString(inMsg));
		logger.info("sendParam----------->" + param.toJSONString());
		ServiceResult exeRemoteApi = remoteApiFactoryImpl
				.exeRemoteApi(remoteApiSelectImpl.getBaseApi(config.getServerName()), cmmd, param);
		if (exeRemoteApi.isSucc()) {
			JSONObject data = JSONObject.parseObject(exeRemoteApi.getData());
			return OutMsgParse.getOutMsg(inMsg, data);
		}
		return null;
	}

}
