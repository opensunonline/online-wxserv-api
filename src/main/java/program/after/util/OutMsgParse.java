package program.after.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;

import program.after.controller.WechatMsgController;
import program.after.entity.wxapi.ServiceConfig;
import program.after.msg.in.InMsg;
import program.after.msg.out.OutMsg;
import program.after.msg.out.OutTextMsg;

/**
 * 返回msg组装
 * @author yangyang.zhang
 * @Package program.after.util 
 * @Date 2018年4月9日 下午4:01:47 
 * @Description TODO(用一句话描述该文件做什么)
 * @version V1.0
 */
public class OutMsgParse {
	
	private static Logger logger = LoggerFactory.getLogger(OutMsgParse.class);
	
	public static boolean isSend(InMsg inMsg, ServiceConfig config) {
		String[] msgType = config.getMsgType().split(",");
		logger.info("msgType=[" + msgType + "]");
		if (msgType == null || msgType.length <= 0) {
			return false;
		}
		String inMsgType = inMsg.getMsgType();
		logger.info("inMsgType=[" + inMsgType + "]");
		for (int i = 0; i < msgType.length; i++) {
			String type = msgType[i];
			if (type.equals(inMsgType)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 获取返回的msg对象
	 * 
	 * @param inMsg
	 * @param data
	 * @return
	 */
	public static OutMsg getOutMsg(InMsg inMsg, JSONObject data) {
		String msgType = data.getString("msgType");
		JSONObject outMsg = data.getJSONObject("outMsg");
		if ("text".equals(msgType))
			return parseInTextMsg(inMsg, outMsg, msgType);
		if ("image".equals(msgType))
			return null;
		if ("voice".equals(msgType))
			return null;
		if ("video".equals(msgType))
			return null;
		if ("shortvideo".equals(msgType))
			return null;
		if ("location".equals(msgType))
			return null;
		if ("link".equals(msgType))
			return null;
		if ("event".equals(msgType))
			return null;
		return null;
	}

	public static OutMsg parseInTextMsg(InMsg inMsg, JSONObject outMsg, String msgType) {
		// TODO Auto-generated method stub
		OutTextMsg outTextMsg = new OutTextMsg(inMsg);
		outTextMsg.setContent(outMsg.getString("content"));
		return outTextMsg;
	}
	
}
