package program.after.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import program.faceapi.base.Common;
import program.faceapi.base.ServiceApiHelper;
import program.faceapi.base.ServiceParam;
import program.faceapi.base.ServiceResult;
import program.faceapi.base.api.BaseApiAdapter;
import program.faceapi.serv.OnlineWxServApi;

@RestController
public class OnlineWxServApiHandler extends BaseApiAdapter implements OnlineWxServApi {

	private static Logger logger = LoggerFactory.getLogger(OnlineWxServApiHandler.class);

	@Override
	public ServiceResult execute(String json) {
		// TODO Auto-generated method stub
		long st = System.currentTimeMillis();
		ServiceResult result = null;
		try {
			logger.info("[" + st + "]exe请求:" + json);
			ServiceParam param = ServiceApiHelper.parseParam(json);
			String hmac1 = param.getHmac(); // 获取签名
			String hmac2 = ServiceApiHelper.md5(param.getData() + Common.KEY);
			if (!hmac2.equals(hmac1)) {
				result = ServiceResult.createFailResult("接口签名验证失败");
				logger.info("[" + st + "]exe响应[" + (System.currentTimeMillis() - st) + "]:"
						+ JSONObject.toJSONString(result));
				return result;
			}
			JSONObject jsonParam = JSONObject.parseObject(param.getData()); // 解析参数
			result = this.exeApi(param.getSysName(), param.getCmmd(), jsonParam); // 执行业务方法
			logger.info("[" + st + "]exe响应[" + (System.currentTimeMillis() - st) + "]-[" + result.getMesg() + "]" + "["
					+ result.isSucc() + "]");
			return result;
		} catch (Exception e) {
			result = ServiceResult.createFailResult("接口处理失败:" + e.getMessage());
			logger.info(
					"[" + st + "]exe失败[" + (System.currentTimeMillis() - st) + "]:" + JSONObject.toJSONString(result));
			return result;
		}
	}

}
