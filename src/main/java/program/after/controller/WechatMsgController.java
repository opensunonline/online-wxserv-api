package program.after.controller;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import program.after.msg.in.InMsg;
import program.after.msg.in.InNotDefinedMsg;
import program.after.msg.in.InTextMsg;
import program.after.msg.in.event.InNotDefinedEvent;
import program.after.msg.in.event.InQrCodeEvent;
import program.after.msg.out.OutMsg;
import program.after.service.MsgSelectService;
import program.after.service.WxMsgUtilService;
import program.after.util.XMLParse;
import program.faceapi.base.controller.BaseController;

/**
 * 所有公众号项目推送消息入口
 * 
 * @author yangyang.zhang
 * @Package program.after.controller
 * @Date 2018年4月7日 下午1:49:18
 * @Description TODO(用一句话描述该文件做什么)
 * @version V1.0
 */
@RestController
@RequestMapping("/msg")
public class WechatMsgController extends BaseController {

	private static Logger logger = LoggerFactory.getLogger(WechatMsgController.class);

	@Autowired
	private WxMsgUtilService wxMsgUtilService;

	@Autowired
	private MsgSelectService msgSelectService;

	@RequestMapping(value = "wxPub/{appId}.html")
	public void msgBack(HttpServletRequest request, HttpServletResponse response, @PathVariable String appId) {
		String resultStr = null;
		logger.info("消息推送接口：appId=[" + appId + "]");
		request.setAttribute("appId", appId);
		try {
			if (wxMsgUtilService.isConfigServerRequest(request)) {
				wxMsgUtilService.configServer(request, response);
				return;
			}
			if (!wxMsgUtilService.checkSignature(request)) {
				logger.info("签名验证失败：" + "appId = " + appId);
				return;
			}
			StringBuilder result = new StringBuilder();
			InputStream in = request.getInputStream();
			byte[] b = new byte[4096];
			for (int n; (n = in.read(b)) != -1;) {
				result.append(new String(b, 0, n, "UTF-8"));
			}
			logger.info(result.toString());
			InMsg inMsg = XMLParse.parse(result.toString());
			OutMsg outMsg = msgSelectService.sendServiceApi(appId, inMsg);
			if (outMsg == null)
				render(response, "success");
			else
				render(response, outMsg.toXml());
		} catch (Exception e) {
			logger.error("公众号通知回调处理异常：appId=[" + appId + "],data=[" + resultStr + "]", e);
		}
	}

	public void render(HttpServletResponse response, OutMsg outMsg) {
		render(response, outMsg.toXml());
	}
}
