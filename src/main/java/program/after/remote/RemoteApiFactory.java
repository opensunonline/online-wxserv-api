package program.after.remote;

import com.alibaba.fastjson.JSONObject;

import program.faceapi.base.ServiceResult;
import program.faceapi.base.api.BaseApi;

public interface RemoteApiFactory {

	public BaseApi getOnlineCenterServApi();

	public BaseApi getOnlineDataApi();

	public BaseApi getOnlineServApi();

	public BaseApi getOnlineWxServApi();
	
	public BaseApi getCollectServiceApi();

	/**
	 * 调用Remote接口公用方法
	 * 
	 * @param api
	 * @param cmmd
	 * @param param
	 * @return
	 */
	public ServiceResult exeRemoteApi(BaseApi api, String cmmd, JSONObject param);

	/**
	 * 调用OnlineService接口公用方法
	 * 
	 * @param cmmd
	 * @param param
	 * @return
	 */
	public ServiceResult exeOnlineServiceApi(String cmmd, JSONObject param);

	/**
	 * 调用OnlineDate接口公用方法
	 * 
	 * @param cmmd
	 * @param param
	 * @return
	 */
	public ServiceResult exeOnlineDateApi(String cmmd, JSONObject param);
}