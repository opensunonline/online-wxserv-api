package program.after.remote;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import program.after.exception.IllegalStateException;
import program.faceapi.base.api.BaseApi;

@Service
public class RemoteApiSelectImpl implements RemoteApiSelect {

	@Autowired
	private RemoteApiFactoryImpl remoteApiFactoryImpl;

	private Map<String, BaseApi> config = new HashMap<>();

	@Override
	public BaseApi getBaseApi(String name) {
		// TODO Auto-generated method stub
		BaseApi api = config.get(name);
		try {
			if (api == null) {
				Method m = (Method) remoteApiFactoryImpl.getClass().getMethod("get" + name);
				api = (BaseApi) m.invoke(remoteApiFactoryImpl);
				if (api == null) {
					throw new IllegalStateException("找不到当前服务API获取方法");
				}
			}
			config.put(name, api);
			return api;
		} catch (Exception e) {
			throw new IllegalStateException("找不到当前服务API获取方法");
		}
	}

}
