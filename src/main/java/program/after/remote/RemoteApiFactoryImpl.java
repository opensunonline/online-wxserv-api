package program.after.remote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import program.faceapi.base.ServiceApiHelper;
import program.faceapi.base.ServiceResult;
import program.faceapi.base.WebSysName;
import program.faceapi.base.api.BaseApi;
import program.faceapi.base.api.SpringContextUtil;
import program.faceapi.data.OnlineDataApi;
import program.faceapi.serv.OnlineCenterServApi;
import program.faceapi.serv.OnlineServApi;
import program.faceapi.serv.OnlineWxServApi;
import program.faceapi.serv.collect.CollectServiceApi;

@Service
public class RemoteApiFactoryImpl implements RemoteApiFactory {

	private static Logger logger = LoggerFactory.getLogger(RemoteApiFactoryImpl.class);

	private static final WebSysName SYSNAME = WebSysName.WXSERV;

	@Autowired
	private OnlineDataApi onlineDataApi;

	@Autowired
	private OnlineServApi onlineServApi;

	@Autowired
	private OnlineCenterServApi onlineCenterServApi;

	@Autowired
	private OnlineWxServApi onlineWxServApi;

	@Autowired
	private CollectServiceApi collectServiceApi;

	public BaseApi getOnlineDataApi() {
		return onlineDataApi;
	}

	public void setOnlineDataApi(OnlineDataApi onlineDataApi) {
		this.onlineDataApi = onlineDataApi;
	}

	public BaseApi getOnlineServApi() {
		return onlineServApi;
	}

	public void setOnlineServApi(OnlineServApi onlineServApi) {
		this.onlineServApi = onlineServApi;
	}

	public BaseApi getOnlineCenterServApi() {
		return onlineCenterServApi;
	}

	public void setOnlineCenterServApi(OnlineCenterServApi onlineCenterServApi) {
		this.onlineCenterServApi = onlineCenterServApi;
	}

	public BaseApi getOnlineWxServApi() {
		return onlineWxServApi;
	}

	public void setOnlineWxServApi(OnlineWxServApi onlineWxServApi) {
		this.onlineWxServApi = onlineWxServApi;
	}

	public BaseApi getCollectServiceApi() {
		return collectServiceApi;
	}

	public void setCollectServiceApi(CollectServiceApi collectServiceApi) {
		this.collectServiceApi = collectServiceApi;
	}

	@Override
	public ServiceResult exeRemoteApi(BaseApi api, String cmmd, JSONObject param) {
		try {
			String json = ServiceApiHelper.formatParam(cmmd, param.toJSONString(), SYSNAME);
			logger.info("请求参数:" + cmmd + json);
			ServiceResult result = api.execute(json);
			return result;
		} catch (Exception e) {
			logger.error("调用接出现异常:" + cmmd + param.toJSONString(), e);
			return ServiceResult.createFailResult("调用接出现异常:" + e.getMessage());
		}
	}

	@Override
	public ServiceResult exeOnlineServiceApi(String cmmd, JSONObject param) {
		try {
			String json = ServiceApiHelper.formatParam(cmmd, param.toJSONString(), SYSNAME);
			logger.info("请求参数:" + cmmd + json.toString());
			ServiceResult result = this.getOnlineServApi().execute(json);
			logger.info("响应参数:" + JSONObject.toJSONString(result));
			return result;
		} catch (Exception e) {
			logger.error("调用接出现异常:" + cmmd + param.toJSONString(), e);
			return ServiceResult.createFailResult("调用接出现异常:" + e.getMessage());
		}

	}

	@Override
	public ServiceResult exeOnlineDateApi(String cmmd, JSONObject param) {
		try {
			String json = ServiceApiHelper.formatParam(cmmd, param.toJSONString(), SYSNAME);
			logger.info("请求参数:" + cmmd + json);
			ServiceResult result = this.getOnlineDataApi().execute(json);
			logger.info("响应参数:" + JSONObject.toJSONString(result));
			return result;
		} catch (Exception e) {
			logger.error("调用接出现异常:" + cmmd + param.toJSONString(), e);
			return ServiceResult.createFailResult("调用接出现异常:" + e.getMessage());
		}
	}

}
