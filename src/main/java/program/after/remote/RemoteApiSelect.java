package program.after.remote;

import program.faceapi.base.api.BaseApi;

public interface RemoteApiSelect {

	public BaseApi getBaseApi(String name);
	
}
